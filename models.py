# -*- coding: utf-8 -*-
"""
遗传算法和粒子群算法实现：
根据营养需求计算每道菜的推荐食用量
----------
利用开源包：
gaft-遗传算法
pyswarm-粒子群算法
----------
created by wen
@2019.11.28
"""
import numpy as np

from gaft import GAEngine
from gaft.components import DecimalIndividual
from gaft.components import Population
from gaft.operators import TournamentSelection
from gaft.operators import UniformCrossover
from gaft.operators import FlipBitMutation

from pyswarm import pso
# import logging


# 基础类
class BaseModel:
    def __init__(self, nutritions=None, amounts=None, target=None, weights=None):
        self.nutritions = np.array(nutritions)
        self.target = np.array(target)
        self.amounts = amounts
        self.weights = weights if weights is not None else np.ones(self.target.shape)

    def _compute_curr_result(self, x):
        return np.dot(self.nutritions.transpose(), x).squeeze()

    def _compute_loss(self, x):
        def _sigmoid(v):
            return 1 / (1 + np.exp(-v))
        x = x.reshape((-1, 1))
        diff = _sigmoid(self._compute_curr_result(x) - self.target) - 0.5
        loss = float(np.log(1.0001 + np.sqrt(np.sum(np.square(diff) * self.weights))))
        return loss

    def run(self):
        pass


# 遗传算法
class GA(BaseModel):
    def __init__(self, nutritions=None, amounts=None, target=None, weights=None):
        BaseModel.__init__(self, nutritions, amounts, target, weights)
        self.engine = None
        self.population = None
        self._make_model()

    def _make_model(self):
        # Define population.
        indv_template = DecimalIndividual(ranges=[(0.1, 1)]*len(self.nutritions), eps=0.001)
        self.population = Population(indv_template=indv_template, size=30)
        self.population.init()

        # Create genetic operators.
        selection = TournamentSelection()
        crossover = UniformCrossover(pc=0.8, pe=0.5)
        mutation = FlipBitMutation(pm=0.1)

        # Create genetic algorithm engine.
        self.engine = GAEngine(population=self.population, selection=selection,
                               crossover=crossover, mutation=mutation)

        # Define fitness function.
        @self.engine.fitness_register
        def fitness(indv):
            x = np.array(indv.solution)
            loss = self._compute_loss(x)
            curr_fitness = 1.0/loss
            return curr_fitness

    def run(self):
        self.engine.run(ng=30)
        best_indv = self.population.best_indv(self.engine.fitness)
        print("实际计算得到的营养元素摄入量：{}".format(str([round(i, 1) for i in self._compute_curr_result(best_indv.solution)])))
        print("目标营养元素的摄入量：      {}".format(str(list(self.target))))
        result = np.multiply(np.array(best_indv.solution), np.array(self.amounts))
        result = [round(i, 1) for i in list(result)]
        return result


# 粒子群算法
class PSO(BaseModel):
    def __init__(self, nutritions=None, amounts=None, target=None, weights=None):
        BaseModel.__init__(self, nutritions, amounts, target, weights)

    def _fitness(self, x):
        x = np.array(x)
        loss = self._compute_loss(x)
        return loss

    def _constraints(self, x):
        # 取权重最大的n个维度作为约束条件，这n个维度的数据必须大于目标数据
        n_cons = int(len(self.target)*0.7)
        sorted_weights = sorted(enumerate(self.weights), key=lambda x: x[1], reverse=True)
        indexes = [i[0] for i in sorted_weights][:n_cons]
        #
        x = np.array(x).reshape((-1, 1))
        diff = self._compute_curr_result(x) - self.target
        selected_diff = list(diff[indexes])
        return selected_diff

    def run(self):
        low_bound = [0.1]*len(self.nutritions)
        high_bound = [1.0]*len(self.nutritions)
        best_x, best_fitness = pso(func=self._fitness, lb=low_bound, ub=high_bound, f_ieqcons=self._constraints)
        print("实际计算得到的营养元素摄入量：{}".format(str([round(i, 1) for i in self._compute_curr_result(best_x)])))
        print("目标营养元素的摄入量：      {}".format(str(list(self.target))))
        result = np.multiply(best_x, self.amounts)
        result = [round(i, 1) for i in list(result)]
        return result


# 调用接口
def get_best_amounts(recipes, required, weights=None, method="pso"):
    """
    :param recipes: 输入菜谱数据， list，每个元素是一个dict，包含营养元素和菜品重量的字段
    :param required: 需要的营养元素量，dict
    :param method: "pso" or "ga"
    :return: 食用量，list
    """
    nutrition_names = list(required.keys())
    nutritions = [[r.get(k, 0) for k in nutrition_names] for r in recipes]
    # todo 菜品重量的字段名待定
    amounts = [r.get("amounts", 0) for r in recipes]
    weights = weights if weights is not None else {}
    weights = [weights.get(k, 1) for k in nutrition_names]
    target = [required.get(k, 0) for k in nutrition_names]
    if "ga" == method:
        model = GA(nutritions=nutritions, amounts=amounts, target=target, weights=weights)
    else:
        model = PSO(nutritions=nutritions, amounts=amounts, target=target, weights=weights)
    best_result = model.run()
    return best_result


if '__main__' == __name__:
    import timeit
    recipes = [
        {
            "energy": 500,
            "protein": 200,
            "fat": 48,
            "carbo": 500,
            "iron": 6.34,
            "amounts": 837
        },
        {
            "energy": 30,
            "protein": 400,
            "fat": 489,
            "carbo": 50,
            "iron": 1.34,
            "amounts": 487
        },
        {
            "energy": 200,
            "protein": 70,
            "fat": 98,
            "carbo": 520,
            "iron": 0.94,
            "amounts": 387
        }
    ]
    required = {
        "energy": 500,
        "protein": 300,
        "fat": 88,
        "carbo": 300,
        "iron": 0.83
    }
    weights = {
        "energy": 0.05,
        "protein": 0.15,
        "fat": 0.5,
        "carbo": 0.15,
        "iron": 0.15
    }
    method = "pso"
    s = timeit.default_timer()
    best = get_best_amounts(recipes=recipes, required=required, weights=None, method=method)
    e = timeit.default_timer()
    print("{} 耗时: {:.5f} 秒.".format(method, e - s))
    print("菜品推荐摄入量/（单位：克）: {}".format(str(best)))
